var idType = context.getVariable("request.queryparam.idType");
var idValue = context.getVariable("request.queryparam.idValue");
var email = context.getVariable("request.queryparam.email");
context.setVariable("isValidRequest", true);

var idTypes =  ["EMAIL","CDG_ID","BILLING_ID","CHECK_DIGIT_ACCOUNT_ID","SFDC_ACCOUNT_ID","SFDC_USER_ID","SFDC_CONTACT_ID","MYACCOUNT_ID","GIGYA_ID"];
var regex = new RegExp(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/);

if (idType === null || idType === "" || idTypes.indexOf(idType) === -1 ) {
    context.setVariable("isValidRequest", false);
    context.setVariable("errorMessage", "Please provide a valid idType");
    context.setVariable("paramName", "idType");
} else if (idValue === null || idValue === "") {
    context.setVariable("isValidRequest", false);
    context.setVariable("errorMessage", "Please provide a valid idValue");
    context.setVariable("paramName", "idValue");
    
}
if (idType === "EMAIL") {
    if (!regex.test(idValue)) {
        context.setVariable("isValidRequest", false);
        context.setVariable("errorMessage", "Please provide a valid idValue");
        context.setVariable("paramName", "idValue");
    }
}
if (email !== null && email !== "") {
    if (!regex.test(email)) {
        context.setVariable("isValidRequest", false);
        context.setVariable("errorMessage", "Please provide a valid email");
        context.setVariable("paramName", "email");
    }
}